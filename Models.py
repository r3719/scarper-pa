from flask_peewee.ath import BaseUser 
from app import Database
import datetime


class User(db.Model, BaseUser):
    username = CharField(unique=True)
    first_name = CharField()
    last_name = CharField()
    email = CharField()
    address = TextField()
    active = BooleanField(default=True)
    admin = BooleanField(default=False)

     def __unicode__(self):
        return self.username


class Availability(Model):
    user = ForeignKeyField(User) #backref='availability'?
    start_date = DateTimeField()
    end_date = DateTimeField()


class Need(Model):
    user = ForeignKeyField(User)
    start_date = DateTimeField()
    end_date = DateTimeField()
